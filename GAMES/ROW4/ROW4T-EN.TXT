row4t(6)                         Games Manual                         row4t(6)



NAME
       row4t - vertical board game: Four in a row

SYNOPSIS
       row4g [Options]
       row4t [Options]

DESCRIPTION
       This  is  a vertical board game.  The program row4g is a graphical pro
       gram. It needs SDL2.  The program row4t is a text  based  program.   It
       eventually needs a terminal (depending on the operating system).

       At  the  start  you are asked for the number of players.  Choose "2" to
       play with a friend, or "1" if you don't have any friends.

       The grit is set up vertically and you can drop chips  into  the  slots.
       Your goal is to get four chips in a row. This can be vertically or hor
       izontally or diagonally. The game can also end with a  draw,  when  all
       columns are full and nobody has won.

       You  can  use  the keyboard or on most systems the mouse to control the
       program.

       Have fun!

OPTIONS
       -h
       --help show a short help

       -l
       --version
              show the license

       -e     English

       -d     German

   Options just for the terminal version row4t.
       -v
       --vt100
              try to use the DEC special graphics charset for the board

       -u
       --utf8 use UTF-8 to draw the board

       -a
       --ascii
              use plain ASCII to draw the board

       Use the options -v, -u, -a if the board isn't shown  correctly.   These
       options are ignored on some systems.

COPYRIGHT
       Copyright (c) 2016-2023 Andreas K. F�rster

       This program is free software: you can redistribute it and/or modify it
       under the terms of the GNU Affero General Public License  as  published
       by  the  Free  Software Foundation, either version 3 of the License, or
       (at your option) any later version.

       This program is distributed in the hope that it  will  be  useful,  but
       WITHOUT  ANY  WARRANTY;  without  even  the  implied  warranty  of MER
       CHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  GNU  Affero
       General Public License for more details.

       You  should  have  received a copy of the GNU Affero General Public Li
       cense along with this program.   If  not,  see  <http://www.gnu.org/li
       censes/>.

SEE ALSO
       https://akfoerster.de/p/row4/



                                  2023-01-09                          row4t(6)
