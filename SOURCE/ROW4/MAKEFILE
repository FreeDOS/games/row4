src = .

CROSS =
CC := $(CROSS)gcc
CFLAGS = -O2 -Wall
CPPFLAGS =
LDFLAGS =
LIBS =
INSTALL = install
WINDRES := $(CROSS)windres
ICON = $(src)/akf.ico

SDL_CONFIG = sdl2-config
SDL_CFLAGS != $(SDL_CONFIG) --cflags
SDL_LDFLAGS != $(SDL_CONFIG) --libs


prefix = /usr/local
exec_prefix = $(prefix)
bindir = $(exec_prefix)/bin
datarootdir = $(prefix)/share
datadir = $(datarootdir)
mandir = $(datarootdir)/man
man6dir = $(mandir)/man6
mandir_de = $(mandir)/de
man6dir_de = $(mandir_de)/man6

all: row4t row4-web.cgi row4g

coverage:
	$(MAKE) CC="gcc -std=gnu89 --coverage" CFLAGS="-O0 -g"

arm:
	$(MAKE) CROSS=arm-linux-gnueabihf- LDFLAGS="-s"

windows: windows64

windows32:
	$(MAKE) row4t.exe row4g.exe row4-web.exe CROSS=i686-w64-mingw32- \
	  LIBS="icon.o" LDFLAGS="-s" \
	  SDL_CONFIG=/usr/local/i686-w64-mingw32/bin/sdl2-config

windows64:
	$(MAKE) row4t.exe row4g.exe row4-web.exe CROSS=x86_64-w64-mingw32- \
	  LIBS="icon.o" LDFLAGS="-s" \
	  SDL_CONFIG=/usr/local/x86_64-w64-mingw32/bin/sdl2-config

dos:
	$(MAKE) row4t.com CC=bcc CFLAGS="-Md" LDFLAGS="-Md -i"


row4t row4t.exe: row4.o logic.o textmode.o messages.o $(LIBS)
	$(CC) textmode.o row4.o logic.o messages.o $(LIBS) $(LDFLAGS) -o $@

row4-web.cgi row4-web.exe: cgi.o logic.o $(LIBS)
	$(CC) cgi.o logic.o $(LIBS) $(LDFLAGS) -o $@

r4sdl.o: $(src)/r4sdl.c $(src)/row4.h $(src)/akftext.h
	-$(CC) -c $(CPPFLAGS) $(CFLAGS) $(SDL_CFLAGS) $(src)/r4sdl.c -o $@

akftext.o: $(src)/akftext.c $(src)/akftext.h
	-$(CC) -c $(CPPFLAGS) $(CFLAGS) $(SDL_CFLAGS) $(src)/akftext.c -o $@

utf8.o: $(src)/utf8.c $(src)/akftext.h
	-$(CC) -c $(CPPFLAGS) $(CFLAGS) $(SDL_CFLAGS) $(src)/utf8.c -o $@

row4g row4g.exe: r4sdl.o row4.o messages.o logic.o akftext.o utf8.o data.o
	-$(CC) r4sdl.o row4.o akftext.o utf8.o messages.o logic.o data.o $(LIBS) \
	$(LDFLAGS) $(SDL_LDFLAGS) -o $@

row4.o logic.o textmode.o messages.o cgi.o: $(src)/row4.h

.c.o:
	$(CC) -c $(CPPFLAGS) $(CFLAGS) $< -o $@

icon.o: $(ICON)
	echo "AppIcon ICON \"$(ICON)\"" >icon.rc
	$(WINDRES) -i icon.rc -o $@

bimport: $(src)/bimport.c
	cc $(src)/bimport.c -o $@

data.c: bimport $(src)/chars20.bmp $(src)/logo.bmp $(src)/disc.bmp
	echo "const int \
	Text_CharacterWidth = 10, \
	Text_CharacterHeight = 20;" > $@
	./bimport Text_Font $(src)/chars20.bmp >> $@
	./bimport B_Logo $(src)/logo.bmp >> $@
	./bimport B_Disc $(src)/disc.bmp >> $@

# DOS

row4t.com: textmode.o row4.o logic.o messages.o bcc-syst.o bccmouse.o \
	   bccenv.o $(LIBS)
	$(CC) textmode.o row4.o logic.o messages.o bcc-syst.o bccmouse.o \
	  bccenv.o $(LIBS) $(LDFLAGS) -o $@

bcc-syst.o bccmouse.o: $(src)/row4.h $(src)/bcc-syst.h


install: install-strip

install-strip: row4t
	test -d $(DESTDIR)$(bindir) || $(INSTALL) -d $(DESTDIR)$(bindir)
	test -d $(DESTDIR)$(man6dir) || $(INSTALL) -d $(DESTDIR)$(man6dir)
	test -d $(DESTDIR)$(man6dir_de) || $(INSTALL) -d $(DESTDIR)$(man6dir_de)
	$(INSTALL) -c -s -m 0755 row4t $(DESTDIR)$(bindir)
	-$(INSTALL) -c -s -m 0755 row4g $(DESTDIR)$(bindir)
	$(INSTALL) -c -m 0644 row4t-en.man $(DESTDIR)$(man6dir)/row4t.6
	$(INSTALL) -c -m 0644 row4t-de.man $(DESTDIR)$(man6dir_de)/row4t.6
	-ln -sf row4t.6 $(DESTDIR)$(man6dir)/row4g.6
	-ln -sf row4t.6 $(DESTDIR)$(man6dir_de)/row4g.6

clean:
	-rm -f $(src)/*~
	-rm -f *.[oO]
	-rm -f *.gcda *.gcno *.gcov
	-rm -f icon.rc
	-rm -f data.h chars.h
	-rm -f data.c
	-rm -f row4? row4-web.cgi *.com *.exe
	-rm -f ROW4 ROW4? ROW4-WEB.CGI *.COM *.EXE
	-rm -f bimport

distclean: clean
	-rm -f $(src)/../linux/amd64/row4? $(src)/../linux/amd64/row4-web.cgi
	-rm -f $(src)/../linux/ia32/row4? $(src)/../linux/ia32/row4-web.cgi
	-rm -f $(src)/../dos/row4?.com
	-rm -f $(src)/../windows/amd64/row4*.exe
	-rm -f $(src)/../windows/ia32/row4*.exe

dist: clean
	$(MAKE) dos
	mv row4t.com $(src)/../dos/
	-rm -f *.o
	$(MAKE) windows32
	mv row4t.exe row4g.exe row4-web.exe $(src)/../windows/ia32/
	-rm -f *.o
	$(MAKE) windows64
	mv row4t.exe row4g.exe row4-web.exe $(src)/../windows/amd64/
	-rm -f *.o icon.rc
	$(MAKE) CC="gcc -m32"
	mv row4t row4g row4-web.cgi $(src)/../linux/ia32/
	-rm -f *.o
	$(MAKE) CC="gcc -m64"
	mv row4t row4g row4-web.cgi $(src)/../linux/amd64/

	-rm -f *.o
	-rm -f bimport data.c icon.rc
	cd ../.. && rm -f row4.zip && zip -r -9 row4.zip row4 \
	    -x "row4/.git/*" row4/.gitignore "*.[oOaA]" "*~"

freedos: clean dos
	-rm -f *.o
	-rm -f -r /tmp/fdos
	mkdir /tmp/fdos
	mkdir /tmp/fdos/APPINFO
	mkdir /tmp/fdos/GAMES
	mkdir /tmp/fdos/GAMES/ROW4
	mkdir /tmp/fdos/LINKS
	mkdir /tmp/fdos/SOURCE
	mkdir /tmp/fdos/SOURCE/ROW4
	cp ../row4.lsm /tmp/fdos/APPINFO/
	mv row4t.com /tmp/fdos/GAMES/ROW4/
	cp ../COPYING.TXT /tmp/fdos/GAMES/ROW4/
	cp * /tmp/fdos/SOURCE/ROW4/
	groff -man -T utf8 row4t-en.man | col -bx | sed 's/\xC2\xA9/(c)/' \
	  | iconv -c -f UTF-8 -t IBM437 | todos \
	  > /tmp/fdos/GAMES/ROW4/ROW4T-EN.TXT
	groff -man -T utf8 row4t-de.man | col -bx | sed 's/\xC2\xA9/(c)/' \
	  | iconv -c -f UTF-8 -t IBM437 | todos \
	  > /tmp/fdos/GAMES/ROW4/ROW4T-DE.TXT
	echo "GAMES\\ROW4\\ROW4T" | todos > /tmp/fdos/LINKS/ROW4T.BAT
	cd /tmp/fdos && zip -9rk ROW4.ZIP APPINFO GAMES LINKS SOURCE
