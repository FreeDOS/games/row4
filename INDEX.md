# Four in a row

vertical board game


## Contributions

NLS specific corrections, updates and submissions should not be 
directly to submitted this project. NLS is maintained at the [FD-NLS](https://github.com/shidel/fd-nls)
project on GitHub. If the project is still actively maintained by it's
developer, it may be beneficial to also submit changes to them directly.

## ROW4.LSM

<table>
<tr><td>title</td><td>Four in a row</td></tr>
<tr><td>version</td><td>2023-08-15</td></tr>
<tr><td>entered&nbsp;date</td><td>2017-10-28</td></tr>
<tr><td>description</td><td>vertical board game</td></tr>
<tr><td>author</td><td>akf@akfoerster.de (Andreas K. Foerster)</td></tr>
<tr><td>maintained&nbsp;by</td><td>akf@akfoerster.de (Andreas K. Foerster)</td></tr>
<tr><td>primary&nbsp;site</td><td>https://akfoerster.de/p/row4/</td></tr>
<tr><td>platforms</td><td>GNU/Linux, Windows, DOS (textmode), Web (CGI)</td></tr>
<tr><td>copying&nbsp;policy</td><td>[GNU Affero General Public License, version 3 or later](LICENSE)</td></tr>
</table>
